package ru.pautova.project_manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pautova.project_manager.entity.Subproject;

import java.util.Optional;

@Repository
public interface SubprojectRepository extends JpaRepository<Subproject, Long> {

    Optional<Subproject> findByName(String name);

    void deleteByName(String name);
}