package ru.pautova.project_manager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.project_manager.converter.Converter;
import ru.pautova.project_manager.dto.TaskDTO;
import ru.pautova.project_manager.dto.request.TaskCreateRequest;
import ru.pautova.project_manager.dto.request.TaskEditRequest;
import ru.pautova.project_manager.entity.Person;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.entity.Subproject;
import ru.pautova.project_manager.entity.Task;
import ru.pautova.project_manager.repository.PersonRepository;
import ru.pautova.project_manager.repository.ProjectRepository;
import ru.pautova.project_manager.repository.SubprojectRepository;
import ru.pautova.project_manager.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class TaskService {

    private final Converter<Task, TaskDTO> converter;
    private final SubprojectRepository subprojectRepository;
    private final ProjectRepository projectRepository;
    private final PersonRepository personRepository;
    private final TaskRepository taskRepository;
    private final PersonService personService;

    public List<TaskDTO> getAll() {
        return taskRepository.findAll()
                .stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }

    public TaskDTO create(TaskCreateRequest request) {

        Project project = projectRepository.findByName(request.getProjectName())
                .orElseThrow(() -> new RuntimeException("Project not found"));

        Subproject subproject = null;
        if (request.getSubprojectName() != null) {
            subproject = subprojectRepository.findByName(request.getSubprojectName())
                    .orElseThrow(() -> new RuntimeException("Subproject not found"));
        }

        Task task = new Task()
                .setType(request.getType())
                .setName(request.getName())
                .setStatus(request.getStatus())
                .setDateCreated(LocalDateTime.now())
                .setDateStatusChanged(LocalDateTime.now())
                .setDescription(request.getDescription())
                .setProject(project)
                .setSubproject(subproject)
                .setAssignee(personService.currentPerson());

        return converter.convert(taskRepository.saveAndFlush(task));
    }

    public TaskDTO updateStatus(String name, TaskStatus status) {
        Optional<Task> taskOptional = taskRepository.findByName(name);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            task.setStatus(status).setDateStatusChanged(LocalDateTime.now());
            return converter.convert(taskRepository.saveAndFlush(task));
        } else throw new RuntimeException("No task found");
    }

    public TaskDTO edit(TaskEditRequest request) {
        Optional<Task> taskOptional = taskRepository.findByName(request.getName());

        if (taskOptional.isPresent()) {

            Project project = projectRepository.findByName(request.getProjectName())
                    .orElseThrow(() -> new RuntimeException("Project not found"));
            Person person = personRepository.findByLogin(request.getAssigneeLogin())
                    .orElseThrow(() -> new RuntimeException("Person not found"));

            Subproject subproject = null;
            if (request.getSubprojectName() != null) {
                subproject = subprojectRepository.findByName(request.getSubprojectName())
                        .orElseThrow(() -> new RuntimeException("Subproject not found"));
            }

            Task task = taskOptional.get();
            task
                    .setType(request.getType())
                    .setName(request.getNewName())
                    .setStatus(request.getStatus())
                    .setDateCreated(request.getDateCreated())
                    .setDateStatusChanged(request.getDateStatusChanged())
                    .setDescription(request.getDescription())
                    .setProject(project)
                    .setSubproject(subproject)
                    .setAssignee(person);

            return converter.convert(taskRepository.saveAndFlush(task));
        } else {
            throw new RuntimeException("No task found");
        }
    }

    public void delete(String name) {

        Task task = taskRepository.findByName(name)
                .orElseThrow(() -> new RuntimeException("No task found"));
        Person person = personService.currentPerson();

        if (task.getAssignee() != person) {
            throw new AccessDeniedException("You can't delete another person's task");
        }

        taskRepository.delete(task);
    }
}