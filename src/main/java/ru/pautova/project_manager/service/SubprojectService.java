package ru.pautova.project_manager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.project_manager.converter.Converter;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.entity.Subproject;
import ru.pautova.project_manager.repository.ProjectRepository;
import ru.pautova.project_manager.repository.SubprojectRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class SubprojectService {

    private final Converter<Subproject, SubprojectDTO> converter;
    private final SubprojectRepository repository;
    private final ProjectRepository projectRepository;

    public List<SubprojectDTO> getAll() {
        return repository.findAll()
                .stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }

    public SubprojectDTO create(String name, Long projectId) {
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Subproject not found"));

        Subproject subproject = repository.saveAndFlush(new Subproject().setName(name).setProject(project));

        return converter.convert(subproject);
    }

    public SubprojectDTO edit(String name, String newName) {
        Optional<Subproject> subprojectOptional = repository.findByName(name);
        if (subprojectOptional.isPresent()) {
            Subproject subproject = subprojectOptional.get();
            subproject.setName(newName);
            return converter.convert(repository.saveAndFlush(subproject));
        } else {
            throw new RuntimeException("No subproject found");
        }
    }

    public void delete(String name) {
        try {
            repository.deleteByName(name);
        } catch (Exception e) {
            throw new RuntimeException("No subproject found");
        }
    }
}