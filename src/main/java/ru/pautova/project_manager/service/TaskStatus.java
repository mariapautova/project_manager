package ru.pautova.project_manager.service;

public enum TaskStatus {
    NEW,
    PROGRESS,
    DONE
}
