package ru.pautova.project_manager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.project_manager.converter.Converter;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.repository.ProjectRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProjectService {

    private final Converter<Project, ProjectDTO> converter;
    private final ProjectRepository repository;

    public List<ProjectDTO> getAll() {
        return repository.findAll()
                .stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }

    public ProjectDTO create(String name) {
        return converter.convert(repository.save(new Project().setName(name)));
    }

    public ProjectDTO edit(String name, String newName) {
        Optional<Project> projectOptional = repository.findByName(name);
        if (projectOptional.isPresent()) {
            Project project = projectOptional.get();
            project.setName(newName);
            return converter.convert(repository.saveAndFlush(project));
        } else {
            throw new RuntimeException("No project found");
        }
    }

    public void delete(String name) {
        try {
            repository.deleteByName(name);
        } catch (Exception e) {
            throw new RuntimeException("No subproject found");
        }
    }
}