package ru.pautova.project_manager.service;

public enum TaskType {
    TECHNICAL,
    MANAGEMENT
}
