package ru.pautova.project_manager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.pautova.project_manager.converter.Converter;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.entity.Person;
import ru.pautova.project_manager.repository.PersonRepository;
import ru.pautova.project_manager.security.PersonDetails;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository repository;
    private final Converter<Person, PersonDTO> converter;

    public Person currentPerson() {
        PersonDetails personDetails = (PersonDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return repository.getReferenceById(personDetails.getId());
    }

    public PersonDTO currentPersonConvert() {
        return converter.convert(currentPerson());
    }
}