package ru.pautova.project_manager.security;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pautova.project_manager.entity.Person;
import ru.pautova.project_manager.repository.PersonRepository;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@EqualsAndHashCode
public class PersonDetailsService implements UserDetailsService {
    private final PersonRepository personRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<Person> personOptional = personRepository.findByLogin(login);
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            return new PersonDetails(
                    person.getId(),
                    person.getLogin(),
                    person.getPassword(),
                    person.getRoles().stream().map(PersonGrantedAuthority::new).collect(Collectors.toList()));
        } else {
            throw new UsernameNotFoundException("No person found");
        }
    }
}