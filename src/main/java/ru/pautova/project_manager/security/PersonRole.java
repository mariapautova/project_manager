package ru.pautova.project_manager.security;

public enum PersonRole {
    ADMIN,
    USER
}