package ru.pautova.project_manager.security;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
@EqualsAndHashCode
public class PersonGrantedAuthority implements GrantedAuthority {
    private final static String PREFIX = "ROLE_";
    private final PersonRole personRole;

    @Override
    public String getAuthority() {
        return PREFIX + personRole.name();
    }
}