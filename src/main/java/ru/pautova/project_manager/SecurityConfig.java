package ru.pautova.project_manager;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static ru.pautova.project_manager.security.PersonRole.ADMIN;
import static ru.pautova.project_manager.security.PersonRole.USER;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/login-processing").permitAll()
                .antMatchers("/personal-area", "/project/list", "/projects_and_subprojects",
                        "/subproject/list", "/task/list", "/task/create", "/task/delete", "/task/update_status")
                .hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/**").hasRole(ADMIN.name())
                .and()
                .formLogin()
                .usernameParameter("login")
                .passwordParameter("password")
                .loginProcessingUrl("/login-processing")
                .defaultSuccessUrl("/personal-area")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}