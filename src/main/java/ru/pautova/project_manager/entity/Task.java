package ru.pautova.project_manager.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.pautova.project_manager.service.TaskStatus;
import ru.pautova.project_manager.service.TaskType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "task")
@Data
@Accessors(chain = true)
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TaskType type;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @Column(name = "date_status_changed")
    private LocalDateTime dateStatusChanged;

    @Column(name = "description")
    private String description;

    @ManyToOne()
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne()
    @JoinColumn(name = "subproject_id")
    private Subproject subproject;

    @ManyToOne()
    @JoinColumn(name = "person_id")
    private Person assignee;
}