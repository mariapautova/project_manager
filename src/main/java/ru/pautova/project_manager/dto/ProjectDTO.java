package ru.pautova.project_manager.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProjectDTO {
    private Long id;
    private String name;
}