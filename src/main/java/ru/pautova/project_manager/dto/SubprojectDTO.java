package ru.pautova.project_manager.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SubprojectDTO {
    private Long id;
    private String name;
    private ProjectDTO project;
}