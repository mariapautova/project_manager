package ru.pautova.project_manager.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.pautova.project_manager.service.TaskStatus;
import ru.pautova.project_manager.service.TaskType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class TaskEditRequest {

    @NotBlank
    private String name;

    @NotBlank
    private TaskType type;

    @NotBlank
    private String newName;

    @NotBlank
    private TaskStatus status;

    @PastOrPresent
    private LocalDateTime dateCreated;

    @PastOrPresent
    private LocalDateTime dateStatusChanged;

    @NotBlank
    private String description;

    @NotBlank
    private String projectName;


    private String subprojectName;

    @NotBlank
    private String assigneeLogin;
}