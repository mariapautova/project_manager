package ru.pautova.project_manager.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.pautova.project_manager.service.TaskStatus;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Accessors(chain = true)
public class TaskUpdateStatusRequest {

    @NotBlank
    private String name;

    @NotBlank
    private TaskStatus status;
}