package ru.pautova.project_manager.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Accessors(chain = true)
public class ProjectDeleteRequest {
    @NotBlank
    private String name;
}