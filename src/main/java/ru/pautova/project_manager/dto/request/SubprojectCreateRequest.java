package ru.pautova.project_manager.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Getter
@Setter
@Accessors(chain = true)
public class SubprojectCreateRequest {

    @NotBlank
    private String name;

    @Positive
    private Long projectId;
}