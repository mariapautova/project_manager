package ru.pautova.project_manager.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.pautova.project_manager.service.TaskStatus;
import ru.pautova.project_manager.service.TaskType;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Accessors(chain = true)
public class TaskCreateRequest {

    @NotBlank
    private TaskType type;

    @NotBlank
    private String name;

    @NotBlank
    private TaskStatus status;

    @NotBlank
    private String description;

    @NotBlank
    private String projectName;

    private String subprojectName;
}