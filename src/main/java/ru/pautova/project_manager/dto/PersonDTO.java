package ru.pautova.project_manager.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PersonDTO {
    private long id;
    private String login;
    private String firstName;
    private String lastName;
}