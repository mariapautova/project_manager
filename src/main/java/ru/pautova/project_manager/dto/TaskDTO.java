package ru.pautova.project_manager.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.pautova.project_manager.service.TaskStatus;
import ru.pautova.project_manager.service.TaskType;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TaskDTO {
    private long id;
    private TaskType type;
    private String name;
    private TaskStatus status;
    private LocalDateTime dateCreated;
    private LocalDateTime dateStatusChanged;
    private String description;
    private ProjectDTO project;
    private SubprojectDTO subproject;
    private PersonDTO assignee;
}