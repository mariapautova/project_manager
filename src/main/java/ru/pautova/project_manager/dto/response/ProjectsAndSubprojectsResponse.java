package ru.pautova.project_manager.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;

import java.util.List;

@AllArgsConstructor
@Getter
public class ProjectsAndSubprojectsResponse {
    List<ProjectDTO> projects;
    List<SubprojectDTO> subprojects;
}