package ru.pautova.project_manager.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProjectDeleteResponse {
    private String deletedProjectName;
}