package ru.pautova.project_manager.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.dto.TaskDTO;
import ru.pautova.project_manager.entity.Task;

@Service
@RequiredArgsConstructor
public class TaskToTaskDTOConverter implements Converter<Task, TaskDTO> {

    private final ProjectToProjectDTOConverter projectConverter;
    private final PersonToPersonDTOConverter personConverter;

    @Override
    public TaskDTO convert(Task source) {
        ProjectDTO projectDTO = projectConverter.convert(source.getProject());

        SubprojectDTO subprojectDTO = null;

        if (source.getSubproject() != null) {
            subprojectDTO = SubprojectToSubprojectDTOConverter.from(source.getSubproject(), projectDTO);
        }
        return new TaskDTO()
                .setId(source.getId())
                .setType(source.getType())
                .setName(source.getName())
                .setStatus(source.getStatus())
                .setDateCreated(source.getDateCreated())
                .setDateStatusChanged(source.getDateStatusChanged())
                .setDescription(source.getDescription())
                .setProject(projectDTO)
                .setSubproject(subprojectDTO)
                .setAssignee(personConverter.convert(source.getAssignee()));
    }
}