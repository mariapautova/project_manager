package ru.pautova.project_manager.converter;

import org.springframework.stereotype.Service;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.entity.Person;

@Service
public class PersonToPersonDTOConverter implements Converter<Person, PersonDTO> {

    @Override
    public PersonDTO convert(Person source) {
        return new PersonDTO()
                .setId(source.getId())
                .setLogin(source.getLogin())
                .setFirstName(source.getFirstName())
                .setLastName(source.getLastName());
    }
}