package ru.pautova.project_manager.converter;

import org.springframework.stereotype.Service;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.entity.Project;

@Service
public class ProjectToProjectDTOConverter implements Converter<Project, ProjectDTO> {
    @Override
    public ProjectDTO convert(Project source) {
        return new ProjectDTO()
                .setId(source.getId())
                .setName(source.getName());
    }
}