package ru.pautova.project_manager.converter;

public interface Converter<S, T> {
    T convert(S source);
}
