package ru.pautova.project_manager.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.entity.Subproject;

@Service
@RequiredArgsConstructor
public class SubprojectToSubprojectDTOConverter implements Converter<Subproject, SubprojectDTO> {

    private final ProjectToProjectDTOConverter converter;

    @Override
    public SubprojectDTO convert(Subproject source) {

        if (source == null) {
            return null;
        }

        return new SubprojectDTO()
                .setId(source.getId())
                .setName(source.getName())
                .setProject(converter.convert(source.getProject()));
    }

    public static SubprojectDTO from(Subproject subproject, ProjectDTO projectDTO) {
        return new SubprojectDTO()
                .setId(subproject.getId())
                .setName(subproject.getName())
                .setProject(projectDTO);
    }
}