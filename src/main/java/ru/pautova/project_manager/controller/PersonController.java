package ru.pautova.project_manager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.service.PersonService;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
public class PersonController {

    private final PersonService service;

    @GetMapping("/personal-area")
    public ResponseEntity<PersonDTO> index() {
        return ok(service.currentPersonConvert());
    }
}