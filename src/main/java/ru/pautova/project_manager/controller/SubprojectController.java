package ru.pautova.project_manager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.dto.request.ProjectDeleteRequest;
import ru.pautova.project_manager.dto.request.ProjectEditRequest;
import ru.pautova.project_manager.dto.request.SubprojectCreateRequest;
import ru.pautova.project_manager.dto.response.SubprojectDeleteResponse;
import ru.pautova.project_manager.service.SubprojectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/subproject")
public class SubprojectController {

    private final SubprojectService service;

    @GetMapping("/list")
    public ResponseEntity<List<SubprojectDTO>> list() {
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("/create")
    public ResponseEntity<SubprojectDTO> create(@RequestBody @Validated SubprojectCreateRequest request) {
        return ResponseEntity.ok(service.create(request.getName(), request.getProjectId()));
    }

    @PostMapping("/delete")
    public ResponseEntity<SubprojectDeleteResponse> delete(@RequestBody @Validated ProjectDeleteRequest request) {

        service.delete(request.getName());
        return ResponseEntity.ok(new SubprojectDeleteResponse(request.getName()));
    }

    @PostMapping("/edit")
    public ResponseEntity<SubprojectDTO> edit(@RequestBody @Validated ProjectEditRequest request) {
        try {
            return ResponseEntity.ok(service.edit(request.getName(), request.getNewName()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}