package ru.pautova.project_manager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.dto.request.ProjectCreateRequest;
import ru.pautova.project_manager.dto.request.ProjectDeleteRequest;
import ru.pautova.project_manager.dto.request.ProjectEditRequest;
import ru.pautova.project_manager.dto.response.ProjectDeleteResponse;
import ru.pautova.project_manager.dto.response.ProjectsAndSubprojectsResponse;
import ru.pautova.project_manager.service.ProjectService;
import ru.pautova.project_manager.service.SubprojectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/project")
public class ProjectController {
    private final ProjectService projectService;
    private final SubprojectService subprojectService;

    @GetMapping("/list")
    public ResponseEntity<List<ProjectDTO>> list() {
        return ResponseEntity.ok(projectService.getAll());
    }

    @GetMapping("/projects_and_subprojects")
    public ResponseEntity<ProjectsAndSubprojectsResponse> all() {
        List<ProjectDTO> projects = projectService.getAll();
        List<SubprojectDTO> subprojects = subprojectService.getAll();
        return ResponseEntity.ok(new ProjectsAndSubprojectsResponse(projects, subprojects));
    }

    @PostMapping("/create")
    public ResponseEntity<ProjectDTO> create(@RequestBody @Validated ProjectCreateRequest request) {
        return ResponseEntity.ok(projectService.create(request.getName()));
    }

    @PostMapping("/delete")
    public ResponseEntity<ProjectDeleteResponse> delete(@RequestBody @Validated ProjectDeleteRequest request) {
        projectService.delete(request.getName());
        return ResponseEntity.ok(new ProjectDeleteResponse(request.getName()));
    }

    @PostMapping("/edit")
    public ResponseEntity<ProjectDTO> edit(@RequestBody @Validated ProjectEditRequest request) {
        try {
            return ResponseEntity.ok(projectService.edit(request.getName(), request.getNewName()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}