package ru.pautova.project_manager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pautova.project_manager.dto.TaskDTO;
import ru.pautova.project_manager.dto.request.TaskCreateRequest;
import ru.pautova.project_manager.dto.request.TaskDeleteRequest;
import ru.pautova.project_manager.dto.request.TaskEditRequest;
import ru.pautova.project_manager.dto.request.TaskUpdateStatusRequest;
import ru.pautova.project_manager.dto.response.TaskDeleteResponse;
import ru.pautova.project_manager.service.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
public class TaskController {

    private final TaskService service;

    @GetMapping("/list")
    public ResponseEntity<List<TaskDTO>> list() {
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("/create")
    public ResponseEntity<TaskDTO> create(@RequestBody @Validated TaskCreateRequest request) {
        try {
            return ResponseEntity.ok(service.create(request));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<TaskDeleteResponse> delete(@RequestBody @Validated TaskDeleteRequest request) {

        service.delete(request.getName());
        return ResponseEntity.ok(new TaskDeleteResponse(request.getName()));
    }

    @PostMapping("/edit")
    public ResponseEntity<TaskDTO> edit(@RequestBody @Validated TaskEditRequest request) {
        try {
            return ResponseEntity.ok(service.edit(request));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/update_status")
    public ResponseEntity<TaskDTO> updateStatus(@RequestBody @Validated TaskUpdateStatusRequest request) {
        try {
            return ResponseEntity.ok(service.updateStatus(request.getName(), request.getStatus()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}