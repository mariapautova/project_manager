package ru.pautova.project_manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.pautova.project_manager.Application;
import ru.pautova.project_manager.SecurityConfig;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.dto.request.ProjectDeleteRequest;
import ru.pautova.project_manager.dto.request.ProjectEditRequest;
import ru.pautova.project_manager.dto.request.SubprojectCreateRequest;
import ru.pautova.project_manager.service.PersonService;
import ru.pautova.project_manager.service.ProjectService;
import ru.pautova.project_manager.service.SubprojectService;

import java.util.Collections;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
@WebMvcTest(SubprojectController.class)
@ContextConfiguration(classes = Application.class)
class SubprojectControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SubprojectService subprojectService;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    public void setUp() {
        SubprojectDTO subprojectDTO = new SubprojectDTO().setId(10L).setName("test subproject");

        when(subprojectService.getAll()).thenReturn(Collections.singletonList(subprojectDTO));
        when(subprojectService.create("test", 10L)).thenReturn(subprojectDTO);
        doNothing().when(subprojectService).delete("test");
        when(subprojectService.edit("test", "newName")).thenReturn(subprojectDTO);
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void list() throws Exception {
        mockMvc.perform(get("/subproject/list"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[{\"id\":10,\"name\":\"test subproject\"}]"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void create() throws Exception {
        SubprojectCreateRequest request = new SubprojectCreateRequest().setName("test").setProjectId(10L);

        mockMvc.perform(post("/subproject/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":10,\"name\":\"test subproject\"}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void delete() throws Exception {
        ProjectDeleteRequest request = new ProjectDeleteRequest().setName("test");

        mockMvc.perform(post("/subproject/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"deletedSubprojectName\":\"test\"}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void edit() throws Exception {
        ProjectEditRequest request = new ProjectEditRequest().setName("test").setNewName("newName");

        mockMvc.perform(post("/subproject/edit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":10,\"name\":\"test subproject\"}"));
    }
}