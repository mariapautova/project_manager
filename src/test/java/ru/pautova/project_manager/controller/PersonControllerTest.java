package ru.pautova.project_manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.pautova.project_manager.Application;
import ru.pautova.project_manager.SecurityConfig;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.service.PersonService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
@WebMvcTest(PersonController.class)
@ContextConfiguration(classes = Application.class)
class PersonControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PersonService personService;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    public void setUp() {
        PersonDTO person = new PersonDTO()
                .setId(1L)
                .setFirstName("test")
                .setLastName("test")
                .setLogin("test");

        when(personService.currentPersonConvert()).thenReturn(person);
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void index() throws Exception {

        mockMvc.perform(get("/personal-area"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"login\": \"test\",\n" +
                        "  \"firstName\": \"test\",\n" +
                        "  \"lastName\": \"test\"\n" +
                        "}"));
    }
}