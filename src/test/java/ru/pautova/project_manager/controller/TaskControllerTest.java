package ru.pautova.project_manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.pautova.project_manager.Application;
import ru.pautova.project_manager.SecurityConfig;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.TaskDTO;
import ru.pautova.project_manager.dto.request.TaskCreateRequest;
import ru.pautova.project_manager.dto.request.TaskDeleteRequest;
import ru.pautova.project_manager.dto.request.TaskEditRequest;
import ru.pautova.project_manager.dto.request.TaskUpdateStatusRequest;
import ru.pautova.project_manager.service.TaskService;
import ru.pautova.project_manager.service.TaskStatus;
import ru.pautova.project_manager.service.TaskType;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
@WebMvcTest(TaskController.class)
@ContextConfiguration(classes = Application.class)
class TaskControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TaskService service;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    public void setUp() {
        ProjectDTO projectDTO = new ProjectDTO().setId(10L).setName("test project");
        PersonDTO personDTO = new PersonDTO()
                .setId(10L)
                .setLogin("test person")
                .setFirstName("test")
                .setLastName("test");
        TaskDTO taskDTO = new TaskDTO()
                .setId(10L)
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProject(projectDTO)
                .setSubproject(null)
                .setDescription("to do test task")
                .setAssignee(personDTO);

        when(service.getAll()).thenReturn(Collections.singletonList(taskDTO));
        when(service.create(any())).thenReturn(taskDTO);
        when(service.edit(any())).thenReturn(taskDTO);
        when(service.updateStatus(any(), any())).thenReturn(taskDTO);
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void list() throws Exception {
        mockMvc.perform(get("/task/list"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[{\"id\":10," +
                                "\"type\":\"MANAGEMENT\"," +
                                "\"name\":\"test task\"," +
                                "\"status\":\"NEW\"," +
                                "\"dateCreated\":\"2022-12-10T10:10:00\"," +
                                "\"dateStatusChanged\":\"2022-12-10T10:10:00\"," +
                                "\"description\":\"to do test task\"," +
                                "\"project\":{\"id\":10,\"name\":\"test project\"}," +
                                "\"subproject\":null," +
                                "\"assignee\":{\"id\":10,\"login\":\"test person\"," +
                                "\"firstName\":\"test\",\"lastName\":\"test\"}}]"));

    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void create() throws Exception {
        TaskCreateRequest createRequest = new TaskCreateRequest()
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDescription("to do test task")
                .setProjectName("test project");

        mockMvc.perform(post("/task/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(createRequest)))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"id\":10," +
                                "\"type\":\"MANAGEMENT\"," +
                                "\"name\":\"test task\"," +
                                "\"status\":\"NEW\"," +
                                "\"dateCreated\":\"2022-12-10T10:10:00\"," +
                                "\"dateStatusChanged\":\"2022-12-10T10:10:00\"," +
                                "\"description\":\"to do test task\"," +
                                "\"project\":{\"id\":10,\"name\":\"test project\"}," +
                                "\"subproject\":null," +
                                "\"assignee\":{\"id\":10,\"login\":\"test person\"," +
                                "\"firstName\":\"test\",\"lastName\":\"test\"}}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void delete() throws Exception {
        TaskDeleteRequest request = new TaskDeleteRequest().setName("test");
        doNothing().when(service).delete("test");

        mockMvc.perform(post("/task/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"deletedTaskName\":\"test\"}"));

    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void edit() throws Exception {
        TaskEditRequest editRequest = new TaskEditRequest()
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test")
                .setName("test2")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProjectName("test project")
                .setSubprojectName("test subproject")
                .setDescription("not to do test task")
                .setAssigneeLogin("test");

        mockMvc.perform(post("/task/edit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(editRequest)))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"id\":10," +
                                "\"type\":\"MANAGEMENT\"," +
                                "\"name\":\"test task\"," +
                                "\"status\":\"NEW\"," +
                                "\"dateCreated\":\"2022-12-10T10:10:00\"," +
                                "\"dateStatusChanged\":\"2022-12-10T10:10:00\"," +
                                "\"description\":\"to do test task\"," +
                                "\"project\":{\"id\":10,\"name\":\"test project\"}," +
                                "\"subproject\":null," +
                                "\"assignee\":{\"id\":10,\"login\":\"test person\"," +
                                "\"firstName\":\"test\",\"lastName\":\"test\"}}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void updateStatus() throws Exception {
        TaskUpdateStatusRequest request = new TaskUpdateStatusRequest().setName("test").setStatus(TaskStatus.DONE);

        mockMvc.perform(post("/task/update_status")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"id\":10," +
                                "\"type\":\"MANAGEMENT\"," +
                                "\"name\":\"test task\"," +
                                "\"status\":\"NEW\"," +
                                "\"dateCreated\":\"2022-12-10T10:10:00\"," +
                                "\"dateStatusChanged\":\"2022-12-10T10:10:00\"," +
                                "\"description\":\"to do test task\"," +
                                "\"project\":{\"id\":10,\"name\":\"test project\"}," +
                                "\"subproject\":null," +
                                "\"assignee\":{\"id\":10,\"login\":\"test person\"," +
                                "\"firstName\":\"test\",\"lastName\":\"test\"}}"));
    }
}