package ru.pautova.project_manager.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.pautova.project_manager.security.PersonDetails;
import ru.pautova.project_manager.security.PersonGrantedAuthority;
import ru.pautova.project_manager.security.PersonRole;


import java.util.Collections;

@Configuration
public class MockSecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        return s -> new PersonDetails(
                1L,
                "test",
                "test",
                Collections.singleton(new PersonGrantedAuthority(PersonRole.ADMIN)));
    }
}

