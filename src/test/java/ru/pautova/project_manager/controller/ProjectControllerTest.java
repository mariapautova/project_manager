package ru.pautova.project_manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.pautova.project_manager.Application;
import ru.pautova.project_manager.SecurityConfig;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.request.ProjectCreateRequest;
import ru.pautova.project_manager.dto.request.ProjectDeleteRequest;
import ru.pautova.project_manager.dto.request.ProjectEditRequest;
import ru.pautova.project_manager.service.PersonService;
import ru.pautova.project_manager.service.ProjectService;
import ru.pautova.project_manager.service.SubprojectService;

import java.util.Collections;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import({SecurityConfig.class, MockSecurityConfig.class})
@WebMvcTest(ProjectController.class)
@ContextConfiguration(classes = Application.class)
class ProjectControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ProjectService projectService;

    @MockBean
    PersonService personService;

    @MockBean
    SubprojectService subprojectService;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    public void setUp() {
        PersonDTO person = new PersonDTO()
                .setId(1L)
                .setFirstName("test")
                .setLastName("test")
                .setLogin("test");
        when(personService.currentPersonConvert()).thenReturn(person);

        ProjectDTO projectDTO = new ProjectDTO().setId(10L).setName("test project");

        when(projectService.getAll()).thenReturn(Collections.singletonList(projectDTO));
        when(subprojectService.getAll()).thenReturn(null);
        when(projectService.create("test")).thenReturn(projectDTO);
        doNothing().when(projectService).delete("test");
        when(projectService.edit("test", "newName")).thenReturn(projectDTO);
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void all() throws Exception {
        mockMvc.perform(get("/project/projects_and_subprojects"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"projects\":[{\"id\":10,\"name\":\"test project\"}],\"subprojects\":null}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void list() throws Exception {
        mockMvc.perform(get("/project/list"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[{\"id\":10,\"name\":\"test project\"}]"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void create() throws Exception {
        ProjectCreateRequest request = new ProjectCreateRequest().setName("test");

        mockMvc.perform(post("/project/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":10,\"name\":\"test project\"}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void delete() throws Exception {
        ProjectDeleteRequest request = new ProjectDeleteRequest().setName("test");

        mockMvc.perform(post("/project/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"deletedProjectName\":\"test\"}"));
    }

    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    @Test
    void edit() throws Exception {
        ProjectEditRequest request = new ProjectEditRequest().setName("test").setNewName("newName");

        mockMvc.perform(post("/project/edit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":10,\"name\":\"test project\"}"));
    }
}