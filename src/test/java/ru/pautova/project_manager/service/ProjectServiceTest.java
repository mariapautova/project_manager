package ru.pautova.project_manager.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pautova.project_manager.converter.ProjectToProjectDTOConverter;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.repository.ProjectRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
class ProjectServiceTest {

    @InjectMocks
    ProjectService service;

    @Mock
    ProjectRepository repository;

    @Mock
    ProjectToProjectDTOConverter converter;

    @Test
    void getAll() {
        Project project = new Project().setId(10L).setName("test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(10L).setName("test project");

        List<Project> projectList = Collections.singletonList(project);
        List<ProjectDTO> dtoList = Collections.singletonList(projectDTO);

        when(repository.findAll()).thenReturn(projectList);
        when(converter.convert(project)).thenReturn(projectDTO);

        List<ProjectDTO> resultList = service.getAll();

        assertNotNull(resultList);
        assertEquals(resultList, dtoList);

        verify(repository, times(1)).findAll();
        verify(converter, times(1)).convert(project);
    }

    @Test
    void create() {
        Project project = new Project().setId(20L).setName("new test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(20L).setName("new test project");

        when(repository.save(new Project().setName("new test project"))).thenReturn(project);
        when(converter.convert(project)).thenReturn(projectDTO);

        ProjectDTO result = service.create("new test project");

        assertNotNull(result);
        assertEquals(result, projectDTO);

        verify(repository, times(1)).save(new Project().setName("new test project"));
        verify(converter, times(1)).convert(project);
    }

    @Test
    void edit() {
        Project project = new Project().setId(1L).setName("first main project");
        Project edited = new Project().setId(1L).setName("edited test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(1L).setName("edited test project");

        when(repository.findByName("first main project")).thenReturn(Optional.of(project));
        when(converter.convert(edited)).thenReturn(projectDTO);
        when(repository.saveAndFlush(any())).thenReturn(project);

        ProjectDTO result = service.edit("first main project", "edited test project");

        assertEquals(result, projectDTO);

        verify(repository, times(1)).findByName("first main project");
        verify(repository, times(1)).saveAndFlush(any());
        verify(converter, times(1)).convert(edited);
    }
}