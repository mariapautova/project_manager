package ru.pautova.project_manager.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pautova.project_manager.converter.ProjectToProjectDTOConverter;
import ru.pautova.project_manager.converter.SubprojectToSubprojectDTOConverter;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.SubprojectDTO;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.entity.Subproject;
import ru.pautova.project_manager.repository.ProjectRepository;
import ru.pautova.project_manager.repository.SubprojectRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
class SubprojectServiceTest {

    @InjectMocks
    SubprojectService subprojectService;

    @Mock
    SubprojectRepository repository;

    @Mock
    SubprojectToSubprojectDTOConverter converter;

    @Mock
    ProjectRepository projectRepository;

    @Test
    void getAll() {
        Subproject subproject = new Subproject().setId(10L).setName("test subproject");
        SubprojectDTO subprojectDTO = new SubprojectDTO().setId(10L).setName("test subproject");

        List<Subproject> subprojectList = Collections.singletonList(subproject);
        List<SubprojectDTO> dtoList = Collections.singletonList(subprojectDTO);

        when(repository.findAll()).thenReturn(subprojectList);
        when(converter.convert(subproject)).thenReturn(subprojectDTO);

        List<SubprojectDTO> resultList = subprojectService.getAll();

        assertNotNull(resultList);
        assertEquals(resultList, dtoList);

        verify(repository, times(1)).findAll();
        verify(converter, times(1)).convert(subproject);
    }

    @Test
    void create() {
        Project project = new Project().setId(30L).setName("new test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(30L).setName("new test project");

        Subproject subproject = new Subproject()
                .setId(30L)
                .setName("new test subproject")
                .setProject(project);
        SubprojectDTO subprojectDTO = new SubprojectDTO()
                .setId(30L)
                .setName("new test subproject")
                .setProject(projectDTO);

        when(repository.saveAndFlush(new Subproject().setName("new test subproject").setProject(project))).thenReturn(subproject);
        when(converter.convert(subproject)).thenReturn(subprojectDTO);
        when(projectRepository.findById(30L)).thenReturn(Optional.of(project));

        SubprojectDTO result = subprojectService.create("new test subproject", 30L);

        assertNotNull(result);
        assertEquals(result, subprojectDTO);

        verify(repository, times(1)).saveAndFlush(new Subproject()
                .setName("new test subproject")
                .setProject(project));
        verify(converter, times(1)).convert(subproject);
    }

    @Test
    void edit() {
        Project project = new Project().setId(1L).setName("first main project");
        ProjectDTO projectDTO = new ProjectDTO().setId(1L).setName("first main project");
        Subproject subproject = new Subproject()
                .setId(1L)
                .setName("first auxiliary project")
                .setProject(project);
        Subproject edited = new Subproject()
                .setId(1L)
                .setName("edited test subproject")
                .setProject(project);
        SubprojectDTO subprojectDTO = new SubprojectDTO()
                .setId(1L)
                .setName("edited test subproject")
                .setProject(projectDTO);

        when(repository.findByName("first auxiliary project")).thenReturn(Optional.of(subproject));
        when(converter.convert(edited)).thenReturn(subprojectDTO);
        when(repository.saveAndFlush(any())).thenReturn(edited);

        SubprojectDTO result = subprojectService.edit("first auxiliary project", "edited test subproject");

        assertEquals(result, subprojectDTO);

        verify(repository, times(1)).findByName("first auxiliary project");
        verify(repository, times(1)).saveAndFlush(any());
        verify(converter, times(1)).convert(edited);
    }
}