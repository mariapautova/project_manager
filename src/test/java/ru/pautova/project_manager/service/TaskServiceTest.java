package ru.pautova.project_manager.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pautova.project_manager.converter.TaskToTaskDTOConverter;
import ru.pautova.project_manager.dto.PersonDTO;
import ru.pautova.project_manager.dto.ProjectDTO;
import ru.pautova.project_manager.dto.TaskDTO;
import ru.pautova.project_manager.dto.request.TaskCreateRequest;
import ru.pautova.project_manager.entity.Person;
import ru.pautova.project_manager.entity.Project;
import ru.pautova.project_manager.entity.Task;
import ru.pautova.project_manager.repository.ProjectRepository;
import ru.pautova.project_manager.repository.TaskRepository;
import ru.pautova.project_manager.security.PersonRole;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
class TaskServiceTest {

    @InjectMocks
    TaskService service;

    @Mock
    PersonService personService;

    @Mock
    TaskRepository repository;

    @Mock
    ProjectRepository projectRepository;

    @Mock
    TaskToTaskDTOConverter converter;

    @Test
    void getAll() {
        Project project = new Project().setId(40L).setName("test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(40L).setName("test project");

        Person person = new Person();
        person.setId(10L);
        person.setLogin("test person");
        person.setFirstName("test");
        person.setLastName("test");
        person.setPassword("test");
        person.setRoles(new HashSet<>(Collections.singletonList(PersonRole.ADMIN)));

        PersonDTO personDTO = new PersonDTO()
                .setId(10L)
                .setLogin("test person")
                .setFirstName("test")
                .setLastName("test");

        Task task = new Task()
                .setId(10L)
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProject(project)
                .setSubproject(null)
                .setDescription("to do test task")
                .setAssignee(person);

        TaskDTO taskDTO = new TaskDTO()
                .setId(10L)
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProject(projectDTO)
                .setSubproject(null)
                .setDescription("to do test task")
                .setAssignee(personDTO);

        List<Task> taskList = Collections.singletonList(task);
        List<TaskDTO> dtoList = Collections.singletonList(taskDTO);

        when(repository.findAll()).thenReturn(taskList);
        when(converter.convert(task)).thenReturn(taskDTO);

        List<TaskDTO> resultList = service.getAll();

        assertNotNull(resultList);
        assertEquals(resultList, dtoList);

        verify(repository, times(1)).findAll();
        verify(converter, times(1)).convert(task);
    }

    @Test
    void create() {
        Project project = new Project().setId(50L).setName("test project");
        ProjectDTO projectDTO = new ProjectDTO().setId(50L).setName("test project");

        Person person = new Person();
        person.setId(20L);
        person.setLogin("test person");
        person.setFirstName("test");
        person.setLastName("test");
        person.setPassword("test");
        person.setRoles(new HashSet<>(Collections.singletonList(PersonRole.ADMIN)));

        PersonDTO personDTO = new PersonDTO()
                .setId(20L)
                .setLogin("test person")
                .setFirstName("test")
                .setLastName("test");

        Task task = new Task()
                .setId(20L)
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProject(project)
                .setSubproject(null)
                .setDescription("to do test task")
                .setAssignee(person);

        TaskDTO taskDTO = new TaskDTO()
                .setId(20L)
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDateCreated(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setDateStatusChanged(LocalDateTime.of(2022, 12, 10, 10, 10))
                .setProject(projectDTO)
                .setSubproject(null)
                .setDescription("to do test task")
                .setAssignee(personDTO);

        TaskCreateRequest taskCreateRequest = new TaskCreateRequest()
                .setType(TaskType.MANAGEMENT)
                .setStatus(TaskStatus.NEW)
                .setName("test task")
                .setDescription("to do test task")
                .setProjectName("test project");

        when(projectRepository.findByName("test project")).thenReturn(Optional.ofNullable(project));
        when(personService.currentPerson()).thenReturn(person);
        when(repository.saveAndFlush(any())).thenReturn(task);
        when(converter.convert(task)).thenReturn(taskDTO);

        TaskDTO result = service.create(taskCreateRequest);

        assertNotNull(result);
        assertEquals(result, taskDTO);

        verify(repository, times(1)).saveAndFlush(any());
        verify(converter, times(1)).convert(task);
        verify(projectRepository, times(1)).findByName("test project");
        verify(personService, times(1)).currentPerson();
    }
}